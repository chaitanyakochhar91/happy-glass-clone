﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawnPlatform : MonoBehaviour {

    [SerializeField]
    LineRenderer lineRenderer;
    [SerializeField]
    EdgeCollider2D edgeCollider2d;
    Rigidbody2D rigidbody2d;

    List<Vector3> _vertices = new List<Vector3>();
    List<Vector2> _colliderPoints = new List<Vector2>();

    public void SetRigidBodyState(bool state)
    {
        if (state)
        {
            if (rigidbody2d == null)
            {
                rigidbody2d = gameObject.AddComponent<Rigidbody2D>();
                rigidbody2d.mass = 1;
                rigidbody2d.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            }
        }
        else
        {
            if(rigidbody2d != null)
            {
                Destroy(rigidbody2d);
                rigidbody2d = null;
            }
        }
 
    }

    public void SetVertices(Vector3[] vertices)
    {
        _vertices.Clear();
        _colliderPoints.Clear();
        _vertices.Capacity = vertices.Length;
        _colliderPoints.Capacity = vertices.Length;
        foreach(Vector3 v in vertices)
        {
            _vertices.Add(v);
            _colliderPoints.Add(v);
        }
        lineRenderer.positionCount = _vertices.Count;
        lineRenderer.SetPositions(_vertices.ToArray());
        edgeCollider2d.points = _colliderPoints.ToArray();
    }

    public void AddVertex(Vector3 vertex)
    {
        _vertices.Add(vertex);
        _colliderPoints.Add(vertex);
        lineRenderer.positionCount = _vertices.Count;
        lineRenderer.SetPositions(_vertices.ToArray());
        edgeCollider2d.points = _colliderPoints.ToArray();
    }

    public void SetColor(Color color)
    {
        lineRenderer.startColor = lineRenderer.endColor = color;
    }
    public void SetColor(Color startColor, Color endColor)
    {
        lineRenderer.startColor = startColor;
        lineRenderer.endColor = endColor;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Water" && rigidbody2d.bodyType != RigidbodyType2D.Kinematic)
        {
            rigidbody2d.bodyType = RigidbodyType2D.Kinematic;
            rigidbody2d.velocity = Vector2.zero;
            rigidbody2d.angularVelocity = 0f;
        }
    }
}
