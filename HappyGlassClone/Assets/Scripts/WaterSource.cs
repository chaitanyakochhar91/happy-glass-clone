﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSource : MonoBehaviour, IResetable {

    public int totalDrops;
    public int dropsPerInterval;
    public float waitBetweenIntervals;

    public Vector2 initialVelocity;
    [SerializeField]
    GameObject dropPrefab;
    List<GameObject> spawnedDrops = new List<GameObject>();
    bool flowStarted = false;
    
    void Awake()
    {
        InitDrops();
    }

    public void StartFlow()
    {
        if(flowStarted)
        {
            return;
        }
        flowStarted = true;
        StartCoroutine(StartFlowCR());
    }

    IEnumerator StartFlowCR()
    {
        WaitForSeconds wait = new WaitForSeconds(waitBetweenIntervals);
        int i = 0;
        Vector3 offset = transform.position;
        
        while (i < totalDrops)
        {
            for(int j = 0; j < 10; j++)
            {
                GameObject GO = spawnedDrops[i + j];
                GO.SetActive(true);
                GO.transform.position = offset;
                GO.GetComponent<Rigidbody2D>().velocity = initialVelocity;
                
            }
            i += 10;
            yield return wait;
        }
        
    }

    public void ResetObject()
    {
        foreach(GameObject drop in spawnedDrops)
        {
            Destroy(drop);
        }
        spawnedDrops.Clear();
        InitDrops();
        flowStarted = false;
    }

    void InitDrops()
    {
        spawnedDrops.Capacity = totalDrops;
        for (int i = 0; i < totalDrops; i++)
        {
            GameObject GO = Instantiate(dropPrefab);
            GO.SetActive(false);
            spawnedDrops.Add(GO);
        }
    }
} 
