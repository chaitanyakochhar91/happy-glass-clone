﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour, IResetable {

    Camera mainCamera;

    void Awake()
    {
        mainCamera = Camera.main;
    }

    [SerializeField]
    GameObject drawnPlatformPrefab;
    [SerializeField]
    WaterSource waterSource;
    List<DrawnPlatform> drawnPlatforms = new List<DrawnPlatform>();

	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
        {
            //Create drawn surface.
            GameObject newPlatform = Instantiate(drawnPlatformPrefab, Vector3.zero, Quaternion.identity);
            drawnPlatforms.Add(newPlatform.GetComponent<DrawnPlatform>());
            return;
        }
        if(Input.GetMouseButton(0))
        {
            //Add points to the drawn surface.
            DrawnPlatform latest = drawnPlatforms[drawnPlatforms.Count - 1];
            Vector3 point = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            point.z = 0;
            latest.AddVertex(point);
            return;
        }
        if(Input.GetMouseButtonUp(0))
        {
            DrawnPlatform latest = drawnPlatforms[drawnPlatforms.Count - 1];
            latest.SetRigidBodyState(true);
            waterSource.StartFlow();
            return;
        }
	}

    public void ResetObject()
    {
        foreach (DrawnPlatform dP in drawnPlatforms)
        {
            Destroy(dP.gameObject);
        }
        drawnPlatforms.Clear();
    }
}
