﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public TouchManager touchManager;
    public WaterSource waterSource;
    public Glass glass;

    public Func<bool> evaluationFunction;

    public Image blocker;
    bool isResetting = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ResetLevel()
    {
        if (isResetting)
        {
            return;
        }
        StartCoroutine(ResetObjects());
    }

    IEnumerator ResetObjects()
    {
        isResetting = true;
        blocker.enabled = true;
        Color invisible = Color.black;
        invisible.a = 0;
        Color visible = Color.black;
        yield return LerpColor(blocker, invisible, visible, 0.3f);
        touchManager.ResetObject();
        waterSource.ResetObject();
        glass.ResetObject();
        yield return LerpColor(blocker, visible, invisible, 0.3f);
        blocker.enabled = false;
        isResetting = false;
    }

    IEnumerator SuccessLoop()
    {
        while (true)
        {
            if(glass.IsGlassFull)
            {
                Debug.LogError("Open the success popup");
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator LerpColor(Image i, Color startColor, Color endColor, float duration)
    {
        i.color = startColor;
        float startTime = Time.time;
        while ((Time.time - startTime) <= duration)
        {
            i.color = Color.Lerp(startColor, endColor, (Time.time - startTime) / duration);
            yield return null;
        }
        i.color = endColor;
    }
}
