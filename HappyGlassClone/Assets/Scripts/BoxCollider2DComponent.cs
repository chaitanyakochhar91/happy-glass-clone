﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCollider2DComponent : MonoBehaviour {

    public Action<Collider2D> onTriggerEnter;

    void OnTriggerEnter2D(Collider2D collider)
    {
        onTriggerEnter.SafeInvoke(collider);
    }
}

public static class ExtensionMethods
{
    public static void SafeInvoke<T>(this Action<T> action, T param)
    {
        if (action != null)
        {
            action(param);
        }
    }
}