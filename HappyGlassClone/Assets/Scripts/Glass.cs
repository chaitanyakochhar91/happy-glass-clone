﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour, IResetable {

    public bool IsGlassFull { get {
            return containedDrops >= targetDrops;
        }
    }
    public int targetDrops;

    int containedDrops;

    [SerializeField]
    BoxCollider2DComponent boxTrigger;
    void Awake()
    {
        boxTrigger.onTriggerEnter += OnGlassEntered;
    }

    void OnGlassEntered(Collider2D collider2d)
    {
        if(collider2d.gameObject.tag == "Water")
        {
            containedDrops++;
        }
    }

    public void ResetObject()
    {
        containedDrops = 0;
    }
}
